import React from 'react';

const CommentList = (props) => {

    let show = [];

    props.item.comments.forEach( item => {
        show.push(
            <tr key={Math.random()}>
                <td style={{width: '35px'}}>
                    <div style={{ width: '30px', height: '30px', backgroundColor :  item.color }}>
                    </div>
                </td>
                <td style={{width: '300px'}}>
                    {item.text}
                </td>
            </tr>);})
    

    return (
        <div>
            <h2>Comments# {props.item.id}</h2>
            <table>
                <tbody>
                    {show}
                </tbody>
            </table>
        </div>
    )
}

export default CommentList;
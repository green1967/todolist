import React from 'react';
import Button from 'react-bootstrap/Button';


const ToDoList = (props) => {

    let show = [];
    let borderStyle;
    props.list.forEach( item => {
        if(item.id === props.curItemId) borderStyle = '3px solid red'; else borderStyle = null;
        show.push(
        <tr key={Math.random()}>
            <td 
                style={{width: '300px', textAlign: 'left', cursor: 'pointer', borderLeft: borderStyle}} 
                onClick={()=>props.onClick(item.id)}>{item.name}
            </td>
            <td 
                style={{width: '25px'}}>
                    <div className="comments-counter">
                        {item.comments.length}
                    </div>
            </td>
            <td>
                <Button onClick={()=>props.deleteClick(item.id)}>Delete</Button>
            </td>
        </tr>);})
    
    return (
        <div>
            <table>
                <tbody>
                    {show}
                </tbody>
            </table>
        </div>
    )
}

export default ToDoList;
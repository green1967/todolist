import React, {Component} from 'react';
import AddToDo from './addtodo/addToDo';
import AddComment from './addcomment/addComment';
import './App.css';

class App extends Component {
  state = {
    list: [],
    commentList: [],
    curItem: {
      id: null,
      name: '',
      comments: [],
    },
    newComment: {
      text: '',
      color: '#000000',
    },
    newItemName:'',
  }

componentDidMount() {

}
  
itemHandleChange = (e) => {
  let { name, value } = e.target;
  this.setState({newItemName: value});
}

itemDeleteHandle = (id) => {
  let tmpArray = []; 
  this.state.list.forEach(item => {if(item.id !== id) tmpArray.push(item)});
  this.setState({list: tmpArray}, ()=> {
    if(tmpArray.length) this.setState({curItem: {...this.state.list[0]}});
    else {
    this.setState((prevState) => ({
    curItem: {
      ...prevState.curItem,
      id: null
    }
  }));
  }
});
}

itemSelectHandle = (id) => {
  this.state.list.forEach(item => {
    if(item.id === id) this.setState({curItem: {...item}});
  })
  
}

itemFormSubmit = () => {
  let newId = Date.parse(new Date());
  let curItem = {
      id: newId,
      name: this.state.newItemName,
      comments: [],
    };
  let newList = [...this.state.list];
  newList.push(curItem);
  this.setState({list: newList});
  this.setState({curItem: {...curItem}});
  this.setState({newItemName:''});
}


commentHandleChange = (e) => {
  let { name, value } = e.target;
  this.setState({newComment: {...this.state.newComment, [name]: value}});
}

commentFormSubmit = () => {
  let newItem = {...this.state.curItem};
  newItem.comments.push(this.state.newComment);
  this.setState({curItem: {...newItem}});
  let list = [...this.state.list];
  list.forEach(item => {
    if(item.id === this.state.curItem.id) item.comments=[...this.state.curItem.comments];
  });
  this.setState({list});
//  this.setState({newComment:{...this.state.newComment, text:''}});
  let resetComment = {
    text: '',
    color: '#000000'
  }
  this.setState({newComment:resetComment});

}

commentColorChangeHandle=(newColor) => {
  this.setState({newComment:{...this.state.newComment, color : newColor}});
}


  render() {

    return (
    <div className="App">
      <div>
      <AddToDo
        list={this.state.list}
        curItemId={this.state.curItem.id}
        itemName={this.state.newItemName} 
        formSubmit={this.itemFormSubmit} 
        handleChange={this.itemHandleChange}
        deleteHandle={this.itemDeleteHandle}
        handleSelect={this.itemSelectHandle}
      />
      </div>
      <div>
      <AddComment
        set={this.state.curItem}
        newComment={this.state.newComment}
        formSubmit={this.commentFormSubmit} 
        handleChange={this.commentHandleChange}
        colorHandle={this.commentColorChangeHandle}
      />
     </div>      
    </div>
    );
  };
}

export default App;

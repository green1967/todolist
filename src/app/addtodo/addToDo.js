import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ToDoList from '../todolist/toDoList';


const AddToDo = (props) => {

    const [validated, setValidated] = useState(false);
  
    const handleSubmit = (event) => {
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
   
    setValidated(true);
    event.preventDefault();
    if(form.checkValidity() === true)  props.formSubmit();
      
    };

    return (
      <div className="add-todo-box">
        <h2>Items</h2>
        <Form inline noValidate validated={validated} onSubmit={handleSubmit}>
                <Form.Group controlId="todo">
                    <Form.Control
                        className="my-1 mr-sm-2"
                        required 
                        type="text" 
                        placeholder="Todo name" 
                        name='name' 
                        value={props.itemName} 
                        onChange={props.handleChange}/>                    
                </Form.Group>
                <Button type="submit" className="my-1">
                    Save
                </Button>
         </Form>
        <ToDoList list={props.list} curItemId={props.curItemId} deleteClick={props.deleteHandle} onClick={props.handleSelect}/>
        </div>
    )


}

export default AddToDo;
import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import CommentList from '../commentlist/commentList';
import { SketchPicker } from 'react-color';

const AddComment = (props) => {

    const [validated, setValidated] = useState(false);
    const [showPicker, setShowPicker] = useState(false);
    
  
    const handleSubmit = (event) => {
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
   
    setValidated(true);
    event.preventDefault();
    if(form.checkValidity() === true)  props.formSubmit();
      
    };

    const onClickHandle = () => {
      setShowPicker(true); 
      ;
    }

    let show=null;
    let colorPicker = <SketchPicker
                        color={props.newComment.color}
                        onChangeComplete={(color)=>{setShowPicker(false);props.colorHandle(color.hex)}}
                      />
    let showColorPicker = showPicker?colorPicker:null;
    if(props.set.id !== null) 
      show= <div className="add-comment-box">
      <CommentList item={props.set} colorClick={props.deleteHandle}/>
      <Form inline noValidate validated={validated} onSubmit={handleSubmit}>
              <Form.Group controlId="todo">
                  <div style={{width: '30px', height: '30px', marginRight: '5px',  backgroundColor : props.newComment.color, cursor: 'pointer'}} 
                    onClick={onClickHandle}>
                  </div>
                  <Form.Control
                      className="my-1 mr-sm-2"
                      required 
                      type="text" 
                      placeholder="Type comment here..." 
                      name='text' 
                      value={props.newComment.text} 
                      onChange={props.handleChange}/>                    
              </Form.Group>
              <Button type="submit" className="my-1">
                  Add new
              </Button>
       </Form>
      
      </div>;
      else show=null;

    return (
      <div style={{display: 'flex'}}>
        <div>
          {show}
        </div>
        <div>
          {showColorPicker}
        </div>
      </div>
    )


}

export default AddComment;